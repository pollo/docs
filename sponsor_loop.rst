Sponsor loops
=============

*Known issue:  Gstreamer currently does not loop `mkv` files;  MPEG-2 seems to
works fine though.*

When not streaming talks, we try and show a video loop giving exposure to the
sponsors that have help us enable the event we are filming.

This sponsor loop is usually a series of slides giving basic infos on the event
and on who is sponsoring it. You can however loop any video you like between
talks.

Creating a simple sponsor loop video
------------------------------------

#. Simple slides can be built in your favorite graphical editing tool (or even
   presentation tool such as LibreOffice Impress). Try and ensure that your
   sponsor logos are in a vector format, as this means that you can scale them
   appropriately without loss of quality.

#. Save each slide as a separate `png` file. Ensure that your slides are all at
   the correct resolution, (we are using 720p so slides should be 1280x720), at
   which point you are now working with raster files (hence it being important
   to nominate your resolution).

#. Import each slide into `kdenlive` (drag and drop the files or import 'clips'
   into your sponsor loop project).

#. Lay out slides on time line by dragging each clip into alternative video time
   lines.

   a. Drag your initial clip into video 1 time line.
   #. Double click on the clip in the time line and specify the start time and
      duration - don't forget to add some time for any transition effects
      (fades, wipes, dissolves etc) that you want.
      *NOTE: for the first slide only specify half the duration of the clip that
      you want.*
   #. Drag a transition (i.e. a dissolve) from the effects menu so that it
      finishes at the same time as your clip (it will snap into place).
   #. Double click on your transition, and specify how long you want it to last.
   #. Drag your next clip into the video 2 timeline. It should 'snap' to the
      start of your transition.
   #. Double click on your clip in the timeline to specify its duration (and
      correct start time if you didn't snap to your transition).
   #. Drag a transition (i.e. a dissolve) from the effects menu so that it
      finishes at the same time as your clip (it will snap into place).
   #. Double click on your transition, and specify how long you want it to last.
   #. In the `Properties` window ensure that transitions from video 2 to video 1
      are checked as 'reversed'.
   #. Repeat for each slide to build up your sponsor loop, alternating clips
      between video 1 and video 2 timelines.
   #. Put a second copy of your first clip at the end of the timeline. Since we
      used half the regular duration for that slide, the loop will join here.

#. Preview your work.

#. Go back and correct your transitions fixing reverse property :-)

#. Render you file.

   a. Select render, then the 'Sponsor Loop' profile (if you have previously set
      one up) otherwise create a new profile for sponsor loop with the following
      settings::

       qscale=%quality aq=%audioquality f=mpegts muxrate=10240000 vcodec=mpeg2video bufsize=1835008 vb=7000k minrate=7000k maxrate=7000k g=15 bf=2

#. View your file and make sure that you are happy with it.

Creating a loop from a static image
-----------------------------------

If you just have a single image, ffmpeg can do this for you quite
simply::

  ffmpeg -loop 1 -i loop.png \
  -c:v mpeg2video -pix_fmt:v yuv420p -qscale:v 2 -qmin:v 2 -qmax:v 7 \
  -keyint_min 0 -bf 0 -g 0 -intra:0 -maxrate:0 90M -t 10 loop.ts

This will create a 10 second loop file.

It can also do effects, like this which fades between colour and
greyscale::

  ffmpeg -loop 1 -i loop.png \
  -filter_complex "[0:v] scale=1280x720 [scaled]; [scaled] hue='s=sin(.5*PI*t)+1' [vid]" \
  -map "[vid]" -c:v mpeg2video -pix_fmt:v yuv420p -qscale:v 2 -qmin:v 2 -qmax:v 7 \
  -keyint_min 0 -bf 0 -g 0 -intra:0 -maxrate:0 90M -t 10 loop.ts
