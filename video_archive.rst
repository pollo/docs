Video archive
=============

You can find our video archive `here`_. If you prefer, you can also watch our
videos on `YouTube`_.

We are using automated tools to generate metadata from our video archive and
then use this data to upload the videos to YouTube. You can find the `scraping
tools here`_ and the `YouTube tools here`_.

.. _here: http://ftp.acc.umu.se/pub/debian-meetings/
.. _YouTube: https://www.youtube.com/channel/UC7SbfAPZf8SMvAxp8t51qtQ
.. _scraping tools here: https://salsa.debian.org/debconf-video-team/archive-meta.git
.. _YouTube tools here: https://salsa.debian.org/debconf-video-team/youtube.git
